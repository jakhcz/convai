import argparse
import numpy as np
import csv
import re
from pypinyin import pinyin, lazy_pinyin, Style

# settings
ap = argparse.ArgumentParser()

ap.add_argument("dataset_csv", type=argparse.FileType('r'), help="")
ap.add_argument("base_csv", type=argparse.FileType('r'), help="")
ap.add_argument("pred_csv", type=argparse.FileType('w'), help="")

args = ap.parse_args()

score_list = []
for line in args.dataset_csv:
    c, q, o = line.strip().split(',')
    o = re.sub("[\s+\.\!\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）【】╮╯▽╰╭★→「」！，❤。～《》：（）【】「」？”“；：、]+", "", o)
    c_pinyin_list = list( map(lambda x: x[0], pinyin(c, style=Style.NORMAL)) )
    o_pinyin_list = list( map(lambda x: x[0], pinyin(o, style=Style.NORMAL)) )
    print(c, c_pinyin_list)
    print(o, o_pinyin_list)
    c_pinyin_str = "".join(c_pinyin_list)
    o_pinyin_str = "".join(o_pinyin_list)

    s = 0

    str_score = 0
    if o_pinyin_str in c_pinyin_str:
        str_score += 1
    s += str_score

    bigram_score = 0
    for i, char in enumerate(o_pinyin_list):
        if i == len(o_pinyin_list)-1:
            break
        bigram = o_pinyin_list[i] + o_pinyin_list[i+1]
        #print(bigram, c_pinyin_str)
        if bigram in c_pinyin_str:
            bigram_score += 1
    s += bigram_score

    print(str_score, bigram_score, s)

    flag = 1
    if "不是" in q or "並非" in q:
        flag = -1
    s *= flag
    #TODO character score
    score_list.append(s)

# read base predictions
base_labels = []
for line in args.base_csv:
    base_labels.append(int(line.strip()))

for i in range(0, len(score_list), 4):
    max_score = -1
    print(score_list[i : i+4])
    q_min = min(score_list[i : i+4])
    for j in range(4):
        s = score_list[i + j]
        if s > max_score:
            max_score = s
            pred_label = j + 1
    if max_score == q_min:
        pred_label = base_labels[int(i/4)]
    args.pred_csv.write("%d\n" % pred_label)
