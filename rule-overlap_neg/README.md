# Rule-based 執行方式
```
python3 overlap_score_neg.py DATASET.csv PRED.csv
```

# Rule-based + Base Model
```
python3 overlap_score_neg_backoff.py DATASET.csv BASE.labels PRED.csv
```
`BASE.labels`是另一個model的預測結果，一題一行1~4 label，沒有題號、沒有CSV header

# 上傳前處理
1. 在PRED.csv第一行加上"answer"

2. 產生題號列表
	
	```
	$ cut -d , -f 1 ~/kaggle4/Kaggle答題-編號不連續-空白.csv > id.csv
	```

3. 合併題號及預測結果
	
	```
	$ paste -d , id.csv PRED.csv > PRED.fixID.csv
	```


# Rule說明
1. String分數

	如果選項字串有出現在內文中，此選項得1分

2. Bigram分數

	對於選項中所有character bigram(相鄰中文字元)，如果此bigram有出現在內文中，則累積1分

3. 總分 = String分數 + Bigram分數

4. 如果問題中包含「不是」或「並非」，則將所有選項的分數乘以-1

5. 輸出分數最高的選項

	使用`overlap_score_neg_backoff.py`時，如果四個選項分數都相同，則輸出另一個model預測的結果
