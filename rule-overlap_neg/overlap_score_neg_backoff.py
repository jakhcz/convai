import argparse
import numpy as np
import csv
import re

# settings
ap = argparse.ArgumentParser()

ap.add_argument("dataset_csv", type=argparse.FileType('r'), help="")
ap.add_argument("base_csv", type=argparse.FileType('r'), help="")
ap.add_argument("pred_csv", type=argparse.FileType('w'), help="")

args = ap.parse_args()

score_list = []
for line in args.dataset_csv:
    c, q, o = line.strip().split(',')
    o = re.sub("[\s+\.\!\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）【】╮╯▽╰╭★→「」！，❤。～《》：（）【】「」？”“；：、]+", "", o)

    s = 0

    str_score = 0
    if o in c:
        str_score += 1
    s += str_score

    """
    char_score = 0
    c_char_set = set(list(c))
    for char in o:
        if char in c_char_set:
            char_score += 1
    s += char_score
    """
    bigram_score = 0
    o_char_list = list(o)
    for i, char in enumerate(o_char_list):
        if i == len(o)-1:
            break
        bigram = o_char_list[i] + o_char_list[i+1]
        if bigram in c:
            bigram_score += 1
    s += bigram_score

    flag = 1
    if "不是" in q or "並非" in q:
        flag = -1
    s *= flag
    #TODO character score
    score_list.append(s)

# read base predictions
base_labels = []
for line in args.base_csv:
    base_labels.append(int(line.strip()))

for i in range(0, len(score_list), 4):
    max_score = -1
    print(score_list[i : i+4])
    q_min = min(score_list[i : i+4])
    for j in range(4):
        s = score_list[i + j]
        if s > max_score:
            max_score = s
            pred_label = j + 1
    if max_score == q_min:
        pred_label = base_labels[int(i/4)]
    args.pred_csv.write("%d\n" % pred_label)
