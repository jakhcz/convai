# ASR 使用方法

hackmd link: https://hackmd.io/VA5viWWkSXCMq2qKngN6aQ?view

## 科大訊飛 API 安裝
1. 註冊
2. 開通語音聽寫服務
3. 下載 SDK (Linux environment needs `libasound-dev` package)

## 修改 SDK 檔案
1. 把 `sample/iat_record_sample/` 裡的兩個檔案丟到訊飛的 `sample/iat_record_sample/`
2. `iat_record_run.c` 裡的 `appid` 改成自己訊飛帳號的
3. 刪掉訊飛的 `sample/iat_record_sample/iat_record_sample.c`
4. cd 到訊飛的 `sample/iat_record_ample/`，`source 64bit_make.sh` 編譯

## 安裝其他套件
- auditok
```shell
git clone https://github.com/amsehili/auditok.git
cd auditok
python setup.py install
```
用途：切音檔

- opencc-python
```shell
pip install opencc-python-reimplemented
```
用途：簡轉繁

- parallel
```
sudo apt-get install parallel
```
用途：shell 平行執行

## 辨識流程

### frame rate 轉換 (if needed)
```shell
./convert_frame_rate <intput_dir> <output_dir>
```

- <input_dir>: 輸入音檔資料夾
- <output_dir>: 輸出轉換好的音檔資料夾

:::warning
如果 frame rate 不是 16khz 的話要先轉換才能丟給 ASR
:::

### 切音檔
```shell
./audio_tokenize.sh <input_dir> <output_dir>
```

- <input_dir>: 輸入音檔資料夾
- <output_dir>: 輸出切好的音檔資料夾

切好的檔名是 `<basename>_{N}.wav`，N 代表 `<basename>.wav` 切出來的第幾個檔

:::warning
如果選項音檔很短就不用切了！
:::

### 辨識
```shell
./run.sh <input_dir> <temporarily_output_dir> <output_csv>
```

- <input_dir>: 輸入切好的音檔資料夾
- <temporarily_output_dir>: 暫時輸出辨識結果資料夾
- <output_csv>: 輸出 csv 檔

:::warning
合併音檔的時候，如果音檔沒有切過，script 最後一步合併成 csv 的地方改成用 `combine_asr_untok.py`
:::

