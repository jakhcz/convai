#! /bin/bash
#
# audio_tokenize.sh
# Copyright (C) 2018 jakhcz <jakhcz@debian>
#
# Distributed under terms of the MIT license.
#

WAV_DIR=$1
OUT_DIR=$2

mkdir -p $OUT_DIR

ls $WAV_DIR | parallel -j 8 \
                auditok -i $WAV_DIR/{} -r 44100 \
                        -n 0.5 -m 60 -s 0.8 -d true \
                        -o $OUT_DIR/{/.}-{N}.wav 

