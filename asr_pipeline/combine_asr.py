#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 jakhcz <jakhcz@debian>
# Distributed under terms of the MIT license.
#

import argparse
from os import listdir
from os.path import join
from opencc import OpenCC

def main():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('in_dir', metavar='in_dir', type=str)
    parser.add_argument('out_f', metavar='out_f', type=argparse.FileType('w'))
    args = parser.parse_args()

    opencc = OpenCC('s2tw')

    fns = listdir(args.in_dir)
    fn_map = dict()
    for fn in fns:
        with open(join(args.in_dir, fn)) as f:
            recog_sent = f.read().strip()
        sp = fn[:-4].strip().split('-')
        fn_map['%s-%03d' % ('-'.join(sp[:-1]), int(sp[-1]))] = recog_sent

    fns = list(fn_map.keys())
    fns.sort()
    prefix = ''
    cur_out = ''
    for fn in fns:
        recog_sent = fn_map[fn]
        if '-'.join(fn.split('-')[:-1]) != prefix:
            if prefix != '':
                cur_out = opencc.convert(cur_out)
                args.out_f.write('"' + prefix + '.wav","' + cur_out + '"\n')
            cur_out = recog_sent
            prefix = '-'.join(fn.split('-')[:-1])
        else:
            cur_out += recog_sent
    cur_out = opencc.convert(cur_out)
    args.out_f.write('"' + prefix + '.wav","' + cur_out + '"\n')


if __name__ == '__main__':
    main()
