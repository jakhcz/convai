#! /bin/bash
#
# convert_frame_rate.sh
# Copyright (C) 2018 jakhcz <jakhcz@debian>
#
# Distributed under terms of the MIT license.
#


WAV_DIR=$1
OUT_DIR=$2

mkdir $OUT_DIR

# FILES=`ls $WAV_DIR`
# for WAV in $FILES; do
#     echo $WAV
#     ffmpeg -i $WAV_DIR/$WAV -ar 16000 $OUT_DIR/$WAV 
# done

ls $WAV_DIR | parallel -j 8 \
                ffmpeg -i $WAV_DIR/{} -ar 16000 $OUT_DIR/{} 

