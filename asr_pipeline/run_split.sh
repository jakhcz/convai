#! /bin/bash
#
# run_split.sh
# Copyright (C) 2018 jakhcz <jakhcz@debian>
#
# Distributed under terms of the MIT license.
#

WAV_DIR=$1
OUT_DIR=$2
OUT_CSV=$3
SPLIT_NO=$4

SPLIT_P=5

# setup environment
cd ./samples/iat_record_sample
export LD_LIBRARY_PATH=$(pwd)/../../libs/x64/
cd ../../

echo "======================="
echo "Source wav dir: $WAV_DIR"
echo "Output txt dir: $OUT_DIR"
echo "Output combined csv: $OUT_CSV"
echo "Split No.: $SPLIT_NO"
echo "======================="

mkdir -p $OUT_DIR

# Recognize empty files (yet completely recognized last time)
find $OUT_DIR -type f -name "*.txt" -empty | parallel -j 16 ./bin/iat_record_run $WAV_DIR/{/.}.wav $OUT_DIR/{/.}.txt

echo "======================="
WAV=($(find $WAV_DIR -type f -name "*.wav"))
WAV_N=${#WAV[@]}
echo "Total wav: $WAV_N"

TXT=($(find $OUT_DIR -type f -name "*.txt"))
TXT_N=${#TXT[@]}
echo "Completed transcript: $TXT_N"

split_sz=$((WAV_N / SPLIT_P))
split_sz=$((split_sz + 1))
split_start=$((SPLIT_NO * split_sz))
split_end=$((split_start + split_sz))
split_end=$(($split_end<$WAV_N?$split_end:$WAV_N))
# echo "$split_start $split_end"
SPLIT=${WAV[@]:$split_start:$split_end}
SPLIT_N=$((split_end - split_start))
echo "Split WAV: $SPLIT_N"

FILE_N=$((SPLIT_N - TXT_N))
echo "Recognizing files: $FILE_N"
echo "======================="

# Recognize the rest
ls -1 $WAV_DIR | tail -n $FILE_N | parallel -j 16 ./bin/iat_record_run $WAV_DIR/{} $OUT_DIR/{/.}.txt

# combine output files to csv
python combine_asr_untok.py $OUT_DIR $OUT_CSV
