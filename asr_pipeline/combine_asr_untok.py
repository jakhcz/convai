#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 jakhcz <jakhcz@debian>
# Distributed under terms of the MIT license.
#

import argparse
from os import listdir
from os.path import join
from opencc import OpenCC

def main():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('in_dir', metavar='in_dir', type=str)
    parser.add_argument('out_f', metavar='out_f', type=argparse.FileType('w'))
    args = parser.parse_args()

    opencc = OpenCC('s2tw')

    fns = listdir(args.in_dir)
    fns.sort()
    for fn in fns:
        with open(join(args.in_dir, fn)) as f:
            recog_sent = f.read().strip()
        recog_sent = opencc.convert(recog_sent)
        args.out_f.write('"' + fn[:-4] + '.wav","' + recog_sent + '"\n')

if __name__ == '__main__':
    main()
