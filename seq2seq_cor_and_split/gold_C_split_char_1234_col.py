import argparse
import numpy as np
import csv
import re

# settings
ap = argparse.ArgumentParser()

ap.add_argument("script_csv", type=argparse.FileType('r'), help="gold script file")
ap.add_argument("out_file", type=argparse.FileType('w'), help="lines with [SEP]")
ap.add_argument("-s", "--skip-col", default=[], help="skip column no. (0-index)")

args = ap.parse_args()

args.script_csv.readline() # skip header
reader = csv.reader(args.script_csv)
qid = 0
for row in reader:
    #print(qid)

    txt_list = []
    for i, txt in enumerate(row[-5:-1]): # first column: ID / last column: answer
        #print(i, txt)
        if i in args.skip_col:
            continue
        txt = txt.strip()
        txt = txt.replace("\n", "")
        txt_list.append(txt)
    #print(txt_list)

    for i, txt in enumerate(txt_list):
        if i > 0:
            args.out_file.write(" ")
        args.out_file.write("[-%d-] " % (i+1))
        char_list = list(txt)

        args.out_file.write(" ".join( list(char_list) ))
    args.out_file.write("\n")

    qid += 1

print(qid)
