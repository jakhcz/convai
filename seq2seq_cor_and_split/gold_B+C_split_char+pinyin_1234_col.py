import argparse
import numpy as np
import csv
import re
from pypinyin import pinyin, lazy_pinyin, Style

# settings
ap = argparse.ArgumentParser()

ap.add_argument("script_csv", type=argparse.FileType('r'), help="gold script file")
ap.add_argument("out_file", type=argparse.FileType('w'), help="lines with [SEP]")
ap.add_argument("-s", "--skip-col", default=[], help="skip column no. (0-index)")

args = ap.parse_args()

args.script_csv.readline() # skip header
reader = csv.reader(args.script_csv)
qid = 0
for row in reader:
    #print(qid)

    txt_list = []
    for i, txt in enumerate(row[2:-1]): # first column: ID / last column: answer
        #print(i, txt)
        if i in args.skip_col:
            continue
        txt = txt.strip()
        txt = txt.replace("\n", "")
        txt_list.append(txt)
    #print(txt_list)
    #TODO pinyin
    for i, txt in enumerate(txt_list):
        if i > 0:
            args.out_file.write(" [-%d-]￨- " % (i))
        char_list = list(txt) 
        token_list = []
        for char in char_list:
            char_pinyin = pinyin(char, style=Style.TONE2)[0][0]
            if char_pinyin == char: # pinyin not labeled
                t = char + "￨" + "-"
            else:
                t = char + "￨" + char_pinyin
            token_list.append(t)
        args.out_file.write(" ".join( list(token_list) ))
    args.out_file.write("\n")

    qid += 1

print(qid)
