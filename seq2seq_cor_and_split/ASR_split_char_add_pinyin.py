import argparse
import numpy as np
import csv
import re
from pypinyin import pinyin, lazy_pinyin, Style

# settings
ap = argparse.ArgumentParser()

ap.add_argument("A_csv", type=argparse.FileType('r'), help="A.csv")
ap.add_argument("B_csv", type=argparse.FileType('r'), help="B.csv")
ap.add_argument("C_csv", type=argparse.FileType('r'), help="C.csv")
ap.add_argument("out_file", type=argparse.FileType('w'), help="lines with [SEP]")

args = ap.parse_args()

qid_obj = {} # id_obj[id] = {"A": "...", "B": "...", "C": "..."}

# read A
prev_txt = None
reader = csv.reader(args.A_csv)
for row in reader:
    f_name, txt = row # fname: A00..01.wav
    qid = int(f_name[1:-4])
    txt = txt.strip().replace("\n", "")
    if len(txt) == 0:
        txt = prev_txt # get text of previous question
    else:
        prev_txt = txt

    if qid not in qid_obj:
        qid_obj[qid] = {}
    qid_obj[qid]["A"] = txt

# read B
reader = csv.reader(args.B_csv)
for row in reader:
    f_name, txt = row # fname: B00..01.wav
    qid = int(f_name[1:-4])
    txt = txt.strip().replace("\n", "")
    
    qid_obj[qid]["B"] = txt

# read C
reader = csv.reader(args.C_csv)
for row in reader:
    f_name, txt = row # fname: C00..01.wav
    qid = int(f_name[1:-4])
    txt = txt.strip().replace("\n", "")
    
    qid_obj[qid]["C"] = txt

#print(qid_obj)

n_q = 0
for qid in sorted(qid_obj.keys()):
    part_list = []
    for key in ["A", "B", "C"]:
        txt = qid_obj[qid][key]
        char_list = list(txt)
        '''
        pinyin_list = list( map(lambda x: x[0], pinyin(txt, style=Style.NORMAL)) )
        print(char_list)
        print(len(char_list))
        print(pinyin_list)
        print(len(pinyin_list))
        '''
        token_list = []
        for char in char_list:
            char_pinyin = pinyin(char, style=Style.TONE2)[0][0]
            if char_pinyin == char: # pinyin not labeled
                t = char + "￨" + "-"
            else:
                t = char + "￨" + char_pinyin
            token_list.append(t)
        part = " ".join(token_list)
        part_list.append(part)
    args.out_file.write(" [SEP]￨- ".join(part_list))
    args.out_file.write("\n")

    n_q += 1

print(n_q)

