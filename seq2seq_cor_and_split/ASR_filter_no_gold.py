import argparse
import csv

# settings
ap = argparse.ArgumentParser()

ap.add_argument("ASR_csv", type=argparse.FileType('r'), help="{A,B,C}.csv")
ap.add_argument("script_csv", type=argparse.FileType('r'), help="gold script")
ap.add_argument("out_csv", type=argparse.FileType('w'), help="filtered `ASR_csv`")

args = ap.parse_args()

qid_set = set()

# load gold question ids
args.script_csv.readline() # skip header
reader = csv.reader(args.script_csv)
for row in reader:
    f_name = row[0]
    #print(f_name)
    qid = int(f_name)
    qid_set.add(qid)

# read ASR CSV
reader = csv.reader(args.ASR_csv)
writer = csv.writer(args.out_csv)
for row in reader:
    f_name, txt = row # fname: B00..01.wav
    qid = int(f_name[1:-4])
    if qid in qid_set:
        writer.writerow(row)

print(len(qid_set))

