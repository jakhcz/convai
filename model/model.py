# 資料預處理
import re
import csv
import sys
import pickle
import numpy as np
from keras.preprocessing import sequence

def w2id(word,d):
    '''
    word to index in wordid_table.
    '''
    try :
        result = d[word]
    except :
        result = 517016   ### len(d)
    return result

def s2idarr(sentence,d) :
    '''
    sentence to index with w2id function.
    '''
    sentence = sentence.split(' ')
    sentence = list(filter(None,sentence))
    result = [w2id(word,d) for word in sentence]
    return np.array(result)

def readans():
    '''
    read answer data generate by human.
    '''
    f = open('/home1/tung/ASR_new/lib/ans/out.csv','r')
    
    context = []
    question = []
    option = []
    label = []
    for row in csv.reader(f):
        c = s2idarr(row[0],wordid_table)
        q = s2idarr(row[1],wordid_table)
        o = s2idarr(row[2],wordid_table)
        context.append(c)
        question.append(q)
        option.append(o)
        label.append(int(row[3]))
    context = np.array(context)
    question = np.array(question)
    option = np.array(option)
    label = np.array(label)

    return context ,question,option,label
    
def readmovieqa() :
    '''
    data generater with movieqa dataset.
    '''
    f = open('./movie.txt','r',encoding='utf8')
    context = []
    question = []
    option = []
    label = []
    for row in f.readlines():
        data = row.split('$++$')
        c = s2idarr(data[0],wordid_table)
        q = s2idarr(data[1],wordid_table)
        o = data[2].split('\t')
        try :
            ans_num =  int(data[3].strip('\n'))
            for i in range(len(o)) :
                o_i = s2idarr(o[i],wordid_table)
                if i == ans_num :
                    label.append(1)
                else :
                    label.append(0)
                context.append(c)
                question.append(q)
                option.append(o_i) 
        except : 
             pass
    context = np.array(context)
    question = np.array(question)
    option = np.array(option)
    label = np.array(label)
    
    return context ,question,option,label

def readmovieqanew() :
    '''
    data generater with movieqa dataset.
    '''
    f = open('./movie.txt','r',encoding='utf8')
    context = []
    question = []
    option = []
    label = []
    for row in f.readlines():
        data = row.split('$++$')
        c = s2idarr(data[0],wordid_table)
        q = s2idarr(data[1],wordid_table)
        o = data[2].split('\t')
        try :
            ans_num =  int(data[3].strip('\n'))
        except :
            continue
        o_t = o.pop(ans_num)
        o_f = o[np.random.randint(0,len(o))]
        o_t = s2idarr(o_t,wordid_table)
        o_f = s2idarr(o_f,wordid_table)
        context.append(c)
        context.append(c)
        question.append(q)
        question.append(q)
        option.append(o_t) 
        option.append(o_f)
        label.append(1)
        label.append(0)
    context = np.array(context)
    question = np.array(question)
    option = np.array(option)
    label = np.array(label)
    
    return context ,question,option,label

def readsquad():
    '''
    data generater with squad dataset.
    '''
    
    f = open('./seg_new.csv','r',encoding='utf8')
    context = []
    question = []
    option = []
    label = []
    for row in csv.reader(f) :
        c,q,o = row
        o += ' 。'
        ff = re.sub(o,'',c)
        ff = ff.split('。')
        n = 0
        while n <= 5 :
            o_f = ff[np.random.randint(0,len(ff))]
            o_fl = re.sub(' ','',o_f)
            n = len(o_fl)
            o_f += '。'
        c = s2idarr(c,wordid_table)
        q = s2idarr(q,wordid_table)
        o = s2idarr(o,wordid_table)
        o_f = s2idarr(o_f,wordid_table)
        context.append(c)
        context.append(c)
        question.append(q)
        question.append(q)
        option.append(o)
        option.append(o_f)
        label.append(1)
        label.append(0)
        
    return np.array(context),np.array(question),np.array(option),np.array(label)


def find_len(seq) :
    '''
    find the maxlength or averagelength.
    '''
    length = [len(s) for s in seq]
    maxlen = max(length)
    avglen = sum(length)/len(seq)
    print(len(seq), 'train sequences')
    print (maxlen,'max length')
    print (avglen,'average length')
    return maxlen,avglen

def padd(context,question,option,length):
    c_maxlen,q_maxlen,o_maxlen = length
    result1 = sequence.pad_sequences(context, maxlen=c_maxlen,padding='post',truncating='pre')
    result2 = sequence.pad_sequences(question, maxlen=q_maxlen,padding='post')
    result3 = sequence.pad_sequences(option, maxlen=o_maxlen,padding='post')
    return result1, result2, result3


def test(filename) :
    '''
    generate testing data
    '''
    context = []
    question = []
    option = []
    f = open(filename,'r')
    data = list(csv.reader(f))

    for row in data :
        c = s2idarr(row[0],wordid_table)
        q = s2idarr(row[1],wordid_table)
        o = s2idarr(row[2],wordid_table)
        context.append(c)
        question.append(q)
        option.append(o)
    return np.array(context),np.array(question),np.array(option)
    
def ans(pred,filepath):
    '''
    use predict to generate the answer.csv
    '''
    cnt = 1
    result = ['ID,Answer']
    for i in range(0,len(pred),4):
        n = np.argmax(pred[i:i+4])
        ansbox = ['1','2','3','4']
       # idx = '{:03}'.format(cnt)
        result.append(str(cnt)+','+ansbox[n])
        cnt += 1
    text = '\n'.join(result)
    s = open(filepath,'w')
    s.write(text)
    s.close()


filew2i = open('./ChineseGloVe_W2I.pickle', 'rb')
filei2v = open('./ChineseGloVe_I2V.pickle', 'rb')

wordid_table = pickle.load(filew2i)
idvector_table = pickle.load(filei2v)
'''

### Training 
print ('Now Training --> Read word embedding : ')
### Read word embedding 
embedding_matrix = [idvector_table[i] for i in range(len(idvector_table))]
embedding_matrix.append(np.zeros(300))
embedding_matrix = np.array(embedding_matrix)
np.save('embedding_matrix',embedding_matrix)

print ('Now Training --> Read word embedding : Done')



print ('Now Training --> Read training Data , Squad only ')
context ,question,option,label = readans()
#context ,question,option,label = readsquad()
print ('Now Training --> Read training Data , Squad only : Done')


print ('Now Training --> Read training Data , Squad + MovieQA ')
c1,q1,o1,l1 = readmovieqanew()
c2 ,q2,o2,l2 = readsquad()
context = np.concatenate([c1,c2],0)
question = np.concatenate([q1,q2],0)
option = np.concatenate([o1,o2],0)
label = np.concatenate([l1,l2],0)
print ('Now Training --> Read training Data , Squad + MovieQA : Done ')



print ('Now Training --> Padding sentence to certain length')

c_maxlen = int(sum(find_len(context)) / 2)
q_maxlen = int(sum(find_len(question)) / 2)
o_maxlen = int(sum(find_len(question)) / 2)
#o_maxlen = int(find_len(option)[1])+10

length = [c_maxlen,q_maxlen,o_maxlen]
np.save('length0.npy',length)
print ('Now Training --> Padding sentence to certain length : Saving length.npy')
context ,question,option = padd(context ,question,option,length)
print ('Now Training --> Padding sentence to certain length : Done')


print ('Now Training --> Create model')
'''
from keras.callbacks import EarlyStopping,ModelCheckpoint
import keras
from keras.layers import *
from keras.models import Input,Model
from keras import backend as K
def QA3(embedding_matrix,length):

    c_maxlen,q_maxlen,o_maxlen = length
    hidden_num = 4
       
    context = Input(shape = (c_maxlen,))
    question = Input(shape = (q_maxlen,))
    option = Input(shape = (o_maxlen,))
    
    Embedding_layer = Embedding(embedding_matrix.shape[0], embedding_matrix.shape[1], weights=[embedding_matrix], trainable=False,mask_zero=True)
    
    context_emb = Embedding_layer(context)
    question_emb = Embedding_layer(question)
    option_emb = Embedding_layer(option)

    context_h = Bidirectional(LSTM(hidden_num, dropout = 0.2, recurrent_dropout = 0.2,return_sequences=True))(context_emb)   ### (1000,10)
    context_h = Bidirectional(LSTM(hidden_num, dropout = 0.2, recurrent_dropout = 0.2,return_sequences=True))(context_h)   ### (1000,10)
    question_h = Bidirectional(LSTM(hidden_num,dropout = 0.2, recurrent_dropout = 0.2,return_sequences=True))(question_emb)   ### (20,10)
    question_h = Bidirectional(LSTM(hidden_num,dropout = 0.2, recurrent_dropout = 0.2,return_sequences=True))(question_h)   ### (20,10)
    option_h = Bidirectional(LSTM(hidden_num,dropout = 0.2, recurrent_dropout = 0.2,return_sequences=True))(option_emb)    ### (30,10)
    option_h = Bidirectional(LSTM(hidden_num,dropout = 0.2, recurrent_dropout = 0.2,return_sequences=True))(option_h)    ### (30,10)
    
    hsim_cq = dot([context_h,question_h],2)    ###(1000,20)
    hsim_co = dot([context_h,option_h],2)      ###(1000,30)
    hsim_qo = dot([question_h,option_h],2)     ###(20,30)

    hsim_cqs = Softmax(axis=1)(hsim_cq)
    hsim_cos = Softmax(axis=1)(hsim_co)
    hsim_qos = Softmax(axis=1)(hsim_qo)

    
    hsim_cq_t = Permute([2,1])(hsim_cq) ### (20,1000)
    hsim_co_t = Permute([2,1])(hsim_co)  ### (30,1000)
    hsim_qo_t = Permute([2,1])(hsim_qo)  ### (30,20)

    hsim_cq_ts = Softmax(axis=1)(hsim_cq_t)
    hsim_co_ts = Softmax(axis=1)(hsim_co_t)
    hsim_qo_ts = Softmax(axis=1)(hsim_qo_t)
     
    sim_cq = dot([context_emb,question_emb],2) # (1000,20)
    sim_co = dot([context_emb,option_emb],2) # (1000,30)
    sim_qo = dot([question_emb,option_emb],2) # (20,30)
    
    sim_cqs = Softmax(axis=1)(sim_cq)
    sim_cos = Softmax(axis=1)(sim_co)
    sim_qos = Softmax(axis=1)(sim_qo)
    
    sim_cq_t = Permute([2,1])(sim_cq)  ### (20,1000)
    sim_co_t = Permute([2,1])(sim_co)  ### (30,1000)
    sim_qo_t = Permute([2,1])(sim_qo)  ### (30,20)

    sim_cq_ts = Softmax(axis=1)(sim_cq_t)
    sim_co_ts = Softmax(axis=1)(sim_co_t)
    sim_qo_ts = Softmax(axis=1)(sim_qo_t)

    
    c_hq = dot([hsim_cq_ts,question_emb],1)  #### (1000,100)
    c_ho = dot([hsim_co_ts,option_emb],1)  ###  (1000,100)
    c_newq = dot([sim_cq_ts,question_emb],1)  #### (1000,100)
    c_newo = dot([sim_co_ts,option_emb],1)  ###  (1000,100)
    c_new = Concatenate(2)([c_newq,c_newo,context_emb,context_h,c_hq,c_ho])   ### (1000,510)

    q_hc = dot([context_emb,hsim_cqs],1)  #### (100,20)
    q_hc = Permute([2,1])(q_hc)    ###  (20,100)
    q_ho = dot([hsim_qo_ts,option_emb],1)  ###  (20,100)
    q_newc = dot([context_emb,sim_cqs],1)  #### (100,20)
    q_newc = Permute([2,1])(q_newc)   ###  (20,100)
    q_newo = dot([sim_qo_ts,option_emb],1)  ###  (20,100)
    q_new = Concatenate(2)([q_newc,q_newo,question_emb,question_h,q_hc,q_ho])  ### (20,510)
    
    
    o_hc = dot([context_emb,hsim_cos],1)  #### (100,30)
    o_hc = Permute([2,1])(o_hc)    ### (30,100)
    o_hq = dot([question_emb,hsim_qos],1)  ###  (100,30)
    o_hq = Permute([2,1])(o_hq)    ### (30,100)
    o_newc = dot([context_emb,sim_cos],1)  #### (100,30)
    o_newc = Permute([2,1])(o_newc)    ### (30,100)
    o_newq = dot([question_emb,sim_qos],1)  ###  (100,30)
    o_newq = Permute([2,1])(o_newq)    ### (30,100)
    o_new = Concatenate(2)([o_newq,o_newc,option_emb,option_h,o_hc,o_hq])   ### (30,510)
    
    c_new = Bidirectional(LSTM(32,dropout = 0.2, recurrent_dropout = 0.2))(c_new)   ###(100,1)
    q_new = Bidirectional(LSTM(32,dropout = 0.2, recurrent_dropout = 0.2))(q_new)   ###(100,1)
    o_new = Bidirectional(LSTM(32,dropout = 0.2, recurrent_dropout = 0.2))(o_new)   ###(100,1)
    
    
    '''
    ### 學長
    fusion = Concatenate(1)([c_new,q_new,o_new])  ### (300,1)
    fusion = Dense(1,activation='sigmoid')(fusion)
    model = Model(inputs=[context,question,option],outputs = fusion)
    model.compile(loss='binary_crossentropy', optimizer='adam',metrics=['accuracy'])
    model.summary()

    '''
    f_cq = dot([c_new,q_new],1)
    f_co = dot([c_new,o_new],1)
    f_oq = dot([o_new,q_new],1)
    f_cq = Dense(64,activation='sigmoid')(f_cq)  ## (1)
#     f_cq = Dense(1,activation='tanh')(f_cq)  ## (1)
    f_co = Dense(64,activation='sigmoid')(f_co)  ## (1)
#     f_co = Dense(1,activation='tanh')(f_co)  ## (1)
    f_oq = Dense(64,activation='sigmoid')(f_oq)  ## (1)
#     f_oq = Dense(1,activation='tanh')(f_oq)  ## (1)
    f_all = Concatenate(1)([f_cq,f_co,f_oq])
    f_all = Dropout(0.3)(f_all)
    f_all = Dense(256,activation='sigmoid')(f_all)
    f_all = Dense(1,activation='sigmoid')(f_all)
    model = Model(inputs=[context,question,option],outputs = f_all)
    model.compile(loss='binary_crossentropy', optimizer='adam',metrics=['accuracy'])
    model.summary()

    
    return model

def QA(embedding_matrix,length):

    c_maxlen,q_maxlen,o_maxlen = length
    hidden_num = 4
       
    context = Input(shape = (c_maxlen,))
    question = Input(shape = (q_maxlen,))
    option = Input(shape = (o_maxlen,))
    
    Embedding_layer = Embedding(embedding_matrix.shape[0], embedding_matrix.shape[1], weights=[embedding_matrix], trainable=False,mask_zero=True)
    
    context_emb = Embedding_layer(context)
    question_emb = Embedding_layer(question)
    option_emb = Embedding_layer(option)

    context_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(context_emb)   ### (1000,10)
    context_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(context_h)   ### (1000,10)
    question_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(question_emb)   ### (20,10)
    question_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(question_h)   ### (20,10)
    option_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(option_emb)    ### (30,10)
    option_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(option_h)    ### (30,10)
    
    hsim_cq = dot([context_h,question_h],2)    ###(1000,20)
    hsim_co = dot([context_h,option_h],2)      ###(1000,30)
    hsim_qo = dot([question_h,option_h],2)     ###(20,30)

    hsim_cqs = Softmax(axis=1)(hsim_cq)
    hsim_cos = Softmax(axis=1)(hsim_co)
    hsim_qos = Softmax(axis=1)(hsim_qo)

    
    hsim_cq_t = Permute([2,1])(hsim_cq) ### (20,1000)
    hsim_co_t = Permute([2,1])(hsim_co)  ### (30,1000)
    hsim_qo_t = Permute([2,1])(hsim_qo)  ### (30,20)

    hsim_cq_ts = Softmax(axis=1)(hsim_cq_t)
    hsim_co_ts = Softmax(axis=1)(hsim_co_t)
    hsim_qo_ts = Softmax(axis=1)(hsim_qo_t)
     
    sim_cq = dot([context_emb,question_emb],2) # (1000,20)
    sim_co = dot([context_emb,option_emb],2) # (1000,30)
    sim_qo = dot([question_emb,option_emb],2) # (20,30)
    
    sim_cqs = Softmax(axis=1)(sim_cq)
    sim_cos = Softmax(axis=1)(sim_co)
    sim_qos = Softmax(axis=1)(sim_qo)
    
    sim_cq_t = Permute([2,1])(sim_cq)  ### (20,1000)
    sim_co_t = Permute([2,1])(sim_co)  ### (30,1000)
    sim_qo_t = Permute([2,1])(sim_qo)  ### (30,20)

    sim_cq_ts = Softmax(axis=1)(sim_cq_t)
    sim_co_ts = Softmax(axis=1)(sim_co_t)
    sim_qo_ts = Softmax(axis=1)(sim_qo_t)

    
    c_hq = dot([hsim_cq_ts,question_emb],1)  #### (1000,100)
    c_ho = dot([hsim_co_ts,option_emb],1)  ###  (1000,100)
    c_newq = dot([sim_cq_ts,question_emb],1)  #### (1000,100)
    c_newo = dot([sim_co_ts,option_emb],1)  ###  (1000,100)
    c_new = Concatenate(2)([c_newq,c_newo,context_emb,context_h,c_hq,c_ho])   ### (1000,510)

    q_hc = dot([context_emb,hsim_cqs],1)  #### (100,20)
    q_hc = Permute([2,1])(q_hc)    ###  (20,100)
    q_ho = dot([hsim_qo_ts,option_emb],1)  ###  (20,100)
    q_newc = dot([context_emb,sim_cqs],1)  #### (100,20)
    q_newc = Permute([2,1])(q_newc)   ###  (20,100)
    q_newo = dot([sim_qo_ts,option_emb],1)  ###  (20,100)
    q_new = Concatenate(2)([q_newc,q_newo,question_emb,question_h,q_hc,q_ho])  ### (20,510)
    
    
    o_hc = dot([context_emb,hsim_cos],1)  #### (100,30)
    o_hc = Permute([2,1])(o_hc)    ### (30,100)
    o_hq = dot([question_emb,hsim_qos],1)  ###  (100,30)
    o_hq = Permute([2,1])(o_hq)    ### (30,100)
    o_newc = dot([context_emb,sim_cos],1)  #### (100,30)
    o_newc = Permute([2,1])(o_newc)    ### (30,100)
    o_newq = dot([question_emb,sim_qos],1)  ###  (100,30)
    o_newq = Permute([2,1])(o_newq)    ### (30,100)
    o_new = Concatenate(2)([o_newq,o_newc,option_emb,option_h,o_hc,o_hq])   ### (30,510)
    
    c_new = Bidirectional(LSTM(32,dropout=0.2, recurrent_dropout=0.2))(c_new)   ###(100,1)
    q_new = Bidirectional(LSTM(32,dropout=0.2, recurrent_dropout=0.2))(q_new)   ###(100,1)
    o_new = Bidirectional(LSTM(32,dropout=0.2, recurrent_dropout=0.2))(o_new)   ###(100,1)
    
    f_cq = dot([c_new,q_new],1)
    f_co = dot([c_new,o_new],1)
    f_oq = dot([o_new,q_new],1)
    f_cq = Dense(32,activation='sigmoid')(f_cq)  ## (1)
#     f_cq = Dense(1,activation='tanh')(f_cq)  ## (1)
    f_co = Dense(32,activation='sigmoid')(f_co)  ## (1)
#     f_co = Dense(1,activation='tanh')(f_co)  ## (1)
    f_oq = Dense(32,activation='sigmoid')(f_oq)  ## (1)
#     f_oq = Dense(1,activation='tanh')(f_oq)  ## (1)
    f_all = Concatenate(1)([f_cq,f_co,f_oq])
    #f_all = Dropout(0.3)(f_all)
    f_all = Dense(64,activation='sigmoid')(f_all)
#     f_all = Dense(32,activation='sigmoid')(f_all)
    f_all = Dense(1,activation='sigmoid')(f_all)
    model = Model(inputs=[context,question,option],outputs = f_all)
    model.compile(loss='binary_crossentropy', optimizer='Adam',metrics=['accuracy'])
    model.summary()

    
    return model

def QA2(embedding_matrix,length):

    c_maxlen,q_maxlen,o_maxlen = length
    hidden_num = 16
       
    context = Input(shape = (c_maxlen,))
    question = Input(shape = (q_maxlen,))
    option = Input(shape = (o_maxlen,))
    
    Embedding_layer = Embedding(embedding_matrix.shape[0], embedding_matrix.shape[1], weights=[embedding_matrix], trainable=False,mask_zero=True)
    
    context_emb = Embedding_layer(context)
    question_emb = Embedding_layer(question)
    option_emb = Embedding_layer(option)

    context_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(context_emb)   ### (1000,10)
    context_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(context_h)   ### (1000,10)
    question_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(question_emb)   ### (20,10)
    question_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(question_h)   ### (20,10)
    option_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(option_emb)    ### (30,10)
    option_h = Bidirectional(LSTM(hidden_num,return_sequences=True,dropout=0.2, recurrent_dropout=0.2))(option_h)    ### (30,10)
    
    hsim_cq = dot([context_h,question_h],2)    ###(1000,20)
    hsim_co = dot([context_h,option_h],2)      ###(1000,30)
    hsim_qo = dot([question_h,option_h],2)     ###(20,30)

    hsim_cqs = Softmax(axis=1)(hsim_cq)
    hsim_cos = Softmax(axis=1)(hsim_co)
    hsim_qos = Softmax(axis=1)(hsim_qo)

    
    hsim_cq_t = Permute([2,1])(hsim_cq) ### (20,1000)
    hsim_co_t = Permute([2,1])(hsim_co)  ### (30,1000)
    hsim_qo_t = Permute([2,1])(hsim_qo)  ### (30,20)

    hsim_cq_ts = Softmax(axis=1)(hsim_cq_t)
    hsim_co_ts = Softmax(axis=1)(hsim_co_t)
    hsim_qo_ts = Softmax(axis=1)(hsim_qo_t)
     
    sim_cq = dot([context_emb,question_emb],2) # (1000,20)
    sim_co = dot([context_emb,option_emb],2) # (1000,30)
    sim_qo = dot([question_emb,option_emb],2) # (20,30)
    
    sim_cqs = Softmax(axis=1)(sim_cq)
    sim_cos = Softmax(axis=1)(sim_co)
    sim_qos = Softmax(axis=1)(sim_qo)
    
    sim_cq_t = Permute([2,1])(sim_cq)  ### (20,1000)
    sim_co_t = Permute([2,1])(sim_co)  ### (30,1000)
    sim_qo_t = Permute([2,1])(sim_qo)  ### (30,20)

    sim_cq_ts = Softmax(axis=1)(sim_cq_t)
    sim_co_ts = Softmax(axis=1)(sim_co_t)
    sim_qo_ts = Softmax(axis=1)(sim_qo_t)

    
    c_hq = dot([hsim_cq_ts,question_emb],1)  #### (1000,100)
    c_ho = dot([hsim_co_ts,option_emb],1)  ###  (1000,100)
    c_newq = dot([sim_cq_ts,question_emb],1)  #### (1000,100)
    c_newo = dot([sim_co_ts,option_emb],1)  ###  (1000,100)
    c_new = Concatenate(2)([c_newq,c_newo,context_emb,context_h,c_hq,c_ho])   ### (1000,510)

    q_hc = dot([context_emb,hsim_cqs],1)  #### (100,20)
    q_hc = Permute([2,1])(q_hc)    ###  (20,100)
    q_ho = dot([hsim_qo_ts,option_emb],1)  ###  (20,100)
    q_newc = dot([context_emb,sim_cqs],1)  #### (100,20)
    q_newc = Permute([2,1])(q_newc)   ###  (20,100)
    q_newo = dot([sim_qo_ts,option_emb],1)  ###  (20,100)
    q_new = Concatenate(2)([q_newc,q_newo,question_emb,question_h,q_hc,q_ho])  ### (20,510)
    
    
    o_hc = dot([context_emb,hsim_cos],1)  #### (100,30)
    o_hc = Permute([2,1])(o_hc)    ### (30,100)
    o_hq = dot([question_emb,hsim_qos],1)  ###  (100,30)
    o_hq = Permute([2,1])(o_hq)    ### (30,100)
    o_newc = dot([context_emb,sim_cos],1)  #### (100,30)
    o_newc = Permute([2,1])(o_newc)    ### (30,100)
    o_newq = dot([question_emb,sim_qos],1)  ###  (100,30)
    o_newq = Permute([2,1])(o_newq)    ### (30,100)
    o_new = Concatenate(2)([o_newq,o_newc,option_emb,option_h,o_hc,o_hq])   ### (30,510)
    
    c_new = Bidirectional(LSTM(32,dropout=0.2, recurrent_dropout=0.2))(c_new)   ###(100,1)
    q_new = Bidirectional(LSTM(32,dropout=0.2, recurrent_dropout=0.2))(q_new)   ###(100,1)
    o_new = Bidirectional(LSTM(32,dropout=0.2, recurrent_dropout=0.2))(o_new)   ###(100,1)
    
    f_cq = dot([c_new,q_new],1)
    f_co = dot([c_new,o_new],1)
    f_oq = dot([o_new,q_new],1)
    f_cq = Dense(32,activation='sigmoid')(f_cq)  ## (1)
#     f_cq = Dense(1,activation='tanh')(f_cq)  ## (1)
    f_co = Dense(32,activation='sigmoid')(f_co)  ## (1)
#     f_co = Dense(1,activation='tanh')(f_co)  ## (1)
    f_oq = Dense(32,activation='sigmoid')(f_oq)  ## (1)
#     f_oq = Dense(1,activation='tanh')(f_oq)  ## (1)
    f_all = Concatenate(1)([f_cq,f_co,f_oq])
    #f_all = Dropout(0.3)(f_all)
    f_all = Dense(64,activation='sigmoid')(f_all)
#     f_all = Dense(32,activation='sigmoid')(f_all)
    f_all = Dense(1,activation='sigmoid')(f_all)
    model = Model(inputs=[context,question,option],outputs = f_all)
    model.compile(loss='binary_crossentropy', optimizer='Adam',metrics=['accuracy'])
    model.summary()

    
    return model    


'''
model = QA2(embedding_matrix,length)
print ('Now Training --> Create model : Done ')

# In[ ]:


filepath="QA2squad-{epoch:02d}-{val_acc:.2f}.hdf5"
callback = [
    EarlyStopping(monitor='val_loss', patience=2, verbose=0),
    ModelCheckpoint(filepath, monitor='val_loss', save_best_only=True, verbose=0)
]
model.fit(x = [context[:3448], question[:3448], option[:3448]], y=label[:3448], batch_size=256, epochs=20, shuffle=True,validation_data=([context[3448:],question[3448:],option[3448:]],label[3448:]),callbacks=callback)
# model.save('w1.h5')

'''
### Testing
def TEST_V1():
    length = np.load('length0.npy').tolist()
    embedding_matrix = np.load('embedding_matrix.npy')
    model = QA(embedding_matrix,length)
    model.load_weights('./weights-improvement-13-0.66.hdf5')
    ct,qt,ot = test('./out.csv')
    ct,qt,ot = padd(ct,qt,ot,length)
    pred = model.predict([ct,qt,ot],verbose=1,batch_size = 512)
    pred = pred.reshape(-1,)
    np.save('p1.npy',pred)
    ans(pred,'./predict.csv')

def TEST_V2():
    length = np.load('length.npy').tolist()
    embedding_matrix = np.load('embedding_matrix.npy')
    model = QA2(embedding_matrix,length)
    model.load_weights('./QA2squad-14-0.99.hdf5')
    ct,qt,ot = test('./out.csv')
    ct,qt,ot = padd(ct,qt,ot,length)
    pred = model.predict([ct,qt,ot],verbose=1,batch_size = 512)
    pred = pred.reshape(-1,)
    np.save('p2.npy',pred)
    ans(pred,'./predict2.csv')
def TEST_V3():
    length = np.load('length2.npy').tolist()
    embedding_matrix = np.load('embedding_matrix.npy')
    model = QA3(embedding_matrix,length)
    model.load_weights('/home1/tung/QA/QACNN/movieqa/QAmodel3-04-0.99.hdf5')
    ct,qt,ot = test('./out.csv')
    ct,qt,ot = padd(ct,qt,ot,length)
    pred = model.predict([ct,qt,ot],verbose=1,batch_size = 512)
    pred = pred.reshape(-1,)
    np.save('p3.npy',pred)
    ans(pred,'./predict3.csv')



def ansg(num):
    cnt = 1
    result = ['ID,Answer']
    for i in range(1500):
        result.append(str(cnt)+','+num)
        cnt += 1
    text = '\n'.join(result)
    s = open('pg.csv','w')
    s.write(text)
    s.close()

if sys.argv[1] == '1' :   
    TEST_V1()
if sys.argv[1] == '2' :
    TEST_V2()
if sys.argv[1] == '3' :
    TEST_V3()
if sys.argv[1] == 'g' :
    ansg(sys.argv[2])
