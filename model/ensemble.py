import argparse
import numpy as np
import csv

# settings
ap = argparse.ArgumentParser()

ap.add_argument("id_csv", type=str, help="ID file")
ap.add_argument("prob_npy", nargs="+", type=str, help="")
ap.add_argument("-o", "--out_file", type=str, help="")

args = ap.parse_args()

# add prob.
pred = np.zeros((6000, ))
for npy_path in args.prob_npy:
    np_arr = np.load(npy_path)
    # normalize
    for i in range(0, len(np_arr), 4):
        p_sum = 0.0
        for j in range(0, 4):
            p_sum += np_arr[i+j]
        if p_sum > 0:
            for j in range(0, 4):
                np_arr[i+j] /= p_sum
        else:
            print("warning")
    pred += np_arr

# output
#f = open('ans_idx.csv')
f = open(args.id_csv)
data_ = list(csv.reader(f))
data_.pop(0)
idx = [row[0] for row in data_]
result = ['ID,Answer']
cnt = 0
for i in range(0,len(pred),4):
    n = np.argmax(pred[i:i+4])
    ansbox = ['1','2','3','4']
   # idx = '{:03}'.format(cnt)
    result.append(idx[cnt]+','+ansbox[n])
    cnt += 1
text = '\n'.join(result)
s = open(args.out_file,'w')
s.write(text)
s.close()
