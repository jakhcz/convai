from pypinyin import pinyin, lazy_pinyin, Style

def list_edit_dis(list1, list2):
    #ref. https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python
    if len(list1) < len(list2):
        return list_edit_dis(list2, list1)

    if len(list2) == 0:
        return len(list1)

    previous_row = range(len(list2) + 1)
    for i, str1 in enumerate(list1):
        current_row = [i + 1]
	#print(current_row)
        for j, str2 in enumerate(list2):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (str1 != str2)
            current_row.append( min(insertions, deletions, substitutions) )
        previous_row = current_row
    
    return previous_row[-1] 

# demo
text1 = "裕商行"
text2 = "玉山銀行"

char_list1 = list( map(lambda x: x[0], pinyin(text1, style=Style.NORMAL)) ) 
char_list2 = list( map(lambda x: x[0], pinyin(text2, style=Style.NORMAL)) )

print(text1, char_list1)
print(text2, char_list2)

print("Edit distance =", list_edit_dis(char_list1, char_list2))
