import csv
f = open('C.csv','r')
data = list(csv.reader(f))
datas = [row[1] for row in data]
one = []
for i,v in enumerate(datas) :
    try :
        one.append(v[0])
    except :
        print (data[i])
        
def find(string_,option_,start,N) :
    '''
    N 用 整個辨識結果的長度
    '''
    N = int(N/2)
    result = None
    end = start + N
    if end > len(string_) :
        end = len(string_)
    for i in range(start,end) :
        if string_[i] == option_ :
            result = i
            break
    return result

two = ['二','2','餓']
three = ['三','3']
four = ['四','4','是']
box = [two,three,four]

def find_all(string_):
    idx = [0]
    for num in range(3) :
        n = None
        c = 0
        start = idx[num]+2
        while n == None and c <= len(box[num])-1 :
            option_ = box[num][c]
            n = find(string_,option_,start,len(string_))
            c += 1
        if n == None :
            idx.append(start)
        else :
            idx.append(n)
    return idx

def get(string_,lis):
    a,b = 0,0
    result = []
    for i in range(len(lis)-1):
        a = b+1
        b = lis[i+1]
        result.append(string_[a:b])
    result.append(string_[b+1:])
    return result

option = []
for v in datas :
    lis = find_all(v)
    option.append('|'.join(get(v,lis)))
print ('\n'.join(option))